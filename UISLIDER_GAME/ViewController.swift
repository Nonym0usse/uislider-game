//
//  ViewController.swift
//  UISLIDER_GAME
//
//  Created by VELLA CYRIL on 17/06/2016.
//  Copyright © 2016 VELLA CYRIL. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    @IBOutlet weak var RandomLabel: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var label: UILabel! //SCORE
   
    @IBAction func sliderValueChanged(sender: UISlider)
    {
        let currentValue = Int(sender.value)
        let gen_num = arc4random() % 100
        self.RandomLabel.text = "\(gen_num)"
        
        if currentValue == gen_num
        {
            self.label.text = "You win 200 pts"
        }
      
    }
  
    override func viewDidLoad()
    {
     super.viewDidLoad()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}