//
//  GameController.swift
//  LE JUSTE NOMBRE
//
//  Created by VELLA CYRIL on 20/06/2016.
//  Copyright © 2016 VELLA CYRIL. All rights reserved.
//

import UIKit

class GameController: UIViewController
{
    var nombreHasard: Int = 0
    
    @IBOutlet weak var champRandom: UIButton!
    @IBOutlet weak var resultat: UILabel!
    @IBOutlet weak var score: UILabel!
    @IBOutlet weak var slider: UISlider!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        choisirNombreAuHasard()
    }

    func choisirNombreAuHasard()
    {
        self.nombreHasard = Int(arc4random_uniform(100)) + 1
    }
    
    @IBAction func randomButton(sender: AnyObject)
    {
        self.resultat.text = "\(choisirNombreAuHasard())"
    }
    
    @IBAction func Myslider(sender: AnyObject)
    {
        let nombre: Int = Int (choisirNombreAuHasard())
        let value: Int = Int(slider.value)
        
        if value < nombre
        {
            resultat.text = "100"
        }
        
        else if value > nombre
        {
            resultat.text = "100"
        }
        
        else
        {
            resultat.text = "200"
        }
        
    }
   
}